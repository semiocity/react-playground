export default function Profile({ name, imageId }) {
    const imageUrl = "https://i.imgur.com/" + imageId + "s.jpg";
    return (
      <div>
        <img className="avatar" src={imageUrl} alt={name} />
        {name}
      </div>
    );
  }
  