import logo from '../logo.svg';

function longueur(chaine) {
    if (!chaine) {
      return 0
    }
    return chaine.length
  
  }
  
  
function Greeting(props) {
    const {city, name, couleur} = props
    return <h1 style={{color:couleur}}>Hello, {name} de {city}! Longueur ville:{longueur(city)}</h1>;
  }
  
  function Goodbye(props) {
    const {formule, name} = props
    return <h1>{formule}, {name}</h1>
  }

  export default function Greetings() {
    return (

    <div className="App">
    <header className="App-header">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Edit <code>src/App.js</code> and save to reload.
      </p>
        Learn React
      <Greeting class="lignetexte" name="Divyesh" couleur="red"/>
      <Greeting class="lignetexte" name="Sarah"  couleur="white"/>
      <Greeting class="lignetexte" name="Taylor" city="Paris"  couleur="green"/>
      <Goodbye formule="Ciao" name="Divyesh" />
      <Goodbye formule="Bye" name="Sarah" />
      <Goodbye formule="Arrivederci" name="Taylor"/>


    </header>
  </div>
  )
    }
  