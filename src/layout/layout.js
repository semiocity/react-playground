import { Outlet, Link } from "react-router-dom";

export default function Layout () {
  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/forms">Forms</Link>
          </li>
          <li>
            <Link to="/gallery">Gallerie</Link>
          </li>
          <li>
            <Link to="/greetings">Greetings</Link>
          </li>
          <li>
            <Link to="/timer">Timer</Link>
          </li>
          <li>
            <Link to="/doggies">Doggies</Link>
          </li>
          {/* <li>
            <Link to="/customhook">Custom Hook</Link>
          </li> */}
          {/* <li>
            <Link to="/contact">Contact</Link>
          </li> */}
        </ul>
      </nav>

      <Outlet />
    </>
  )
};
