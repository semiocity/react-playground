import {useState, useEffect, useRef} from 'react';
import './doggies.css';



export default function Doggies() {
    let [dogImage, setDogImage] = useState([])
    let [updateImage, setUpdateImage] = useState(0)
    const currentCount = useRef(0)
    const count = useRef(0);



    // 3. Create out useEffect function
  useEffect(() => {
    fetch("https://dog.ceo/api/breeds/image/random/3")
    .then(response => response.json())
        // 4. Setting *dogImage* to the image url that we received from the response above
    .then(data => {
      setDogImage(data.message)
      count.current = count.current + 1;
      console.log(data)
    })
    
  },[updateImage])

  return (
    <div className="App">
        {/* 5. Using *dogImage as* the *src* for our image*/}
        <div>
            <button onClick={()=> setUpdateImage(updateImage +1)}>Update Images</button>
            <h1>Render Count: {count.current}</h1>
        </div>
        <div>
            <button onClick={()=> console.log("Valeur:", currentCount.current)||(currentCount.current=currentCount.current+1)}>Update Images</button>
            <h1>Render Current Count: {currentCount.current}</h1>
        </div>

    {dogImage.map((dog, index) => 
      <div class="photo"><img key={dog} src={dog}></img><figcaption>{index}</figcaption></div>

    
    )
    }
    </div>
  );
}