import { people } from "../data"
import Profile from "../layout/profile";
import Nombre from "../layout/nombre";

export default function Gallery() {
  return (
    <section>
      <h1>Amazing scientists</h1>
      {people.map((person) => (
        <Profile key={person.id} name={person.name} imageId={person.imageId} />
      ))}
      {/* <p>Nombre de scientifiques: {people.length}</p> */}
      <Nombre people={people} />
      {Nombre({ people })}
    </section>
  );
}
