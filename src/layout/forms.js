import { useState } from "react";
import ReactDOM from 'react-dom';

export default function MyForm() {
    const [name, setName] = useState("");
  
    const handleSubmit = (event) => {
      event.preventDefault();
      alert(`The name you entered was: ${name}`)
    }
  
    return (
      <form onSubmit={handleSubmit}>
        <label>Enter your name:
          <input 
            type="text" 
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
        </label>
        <p>Votre nom est {name}</p>
        <input type="submit" value="Afficher le nom"/>
      </form>
    )
  }
  
//   ReactDOM.render(<MyForm />, document.getElementById('root'));