import './App.css';
import MyForm from './layout/forms'
import Gallery from './layout/gallery';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Layout from "./layout/layout";
import Home from './layout/home';
import Greetings from './layout/greetings';
import Timer from './layout/timer';
import Doggies from './layout/doggies';


function App() {
  return (
    <div>
    <div>
      <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route index element={<Home />} />
          <Route path="forms" element={<MyForm />} />
          <Route path="gallery" element={<Gallery />} />
          <Route path="greetings" element={<Greetings />} />
          <Route path="timer" element={<Timer />} />
          <Route path="doggies" element={<Doggies />} />
          {/* <Route path="customhook" element={<Customhook />} /> */}
          
          {/* <Route path="contact" element={<Contact />} />
          <Route path="*" element={<NoPage />} /> */}
        </Route>
      </Routes>
    </BrowserRouter>
  </div>
    {/* <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
          Learn React
        <Greeting class="lignetexte" name="Divyesh" couleur="red"/>
        <Greeting class="lignetexte" name="Sarah"  couleur="white"/>
        <Greeting class="lignetexte" name="Taylor" city="Paris"  couleur="green"/>
        <Goodbye formule="Ciao" name="Divyesh" />
        <Goodbye formule="Bye" name="Sarah" />
        <Goodbye formule="Arrivederci" name="Taylor"/>
        <Gallery/>
        <MyForm/>


      </header>
    </div> */}
    </div>
  );
}

export default App;
